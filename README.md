# cartography_api_json_server

## Getting started

```
git clone https://gitlab.com/iles-de-paix/cartography_api_json_server.git
```

```js
npm start
cd cartography_api_json_server
npm i
```

## Add your files

- [ ] [Create](https://docs.gitlab.com/ee/user/project/repository/web_editor.html#create-a-file) or [upload](https://docs.gitlab.com/ee/user/project/repository/web_editor.html#upload-a-file) files
- [ ] [Add files using the command line](https://docs.gitlab.com/ee/gitlab-basics/add-file.html#add-a-file-using-the-command-line) or push an existing Git repository with the following command:

```
cd existing_repo
git remote add origin https://gitlab.com/iles-de-paix/cartography_api_json_server.git
git branch -M main
git push -uf origin main
```

## Cartography UI

See [CARTOGRAPHY UI](https://cartography-ui.vercel.app/) deployed in VERCEL: **https://cartography-ui.vercel.app/**

Source code in [GITLAB](https://gitlab.com/iles-de-paix/cartography_ui): **https://gitlab.com/iles-de-paix/cartography_ui**

## Cartography API (JSON Server)

- See [CARTOGRAPHY API](https://cartography-api-json-server.vercel.app/) deployed in VERCEL: **https://cartography-api-json-server.vercel.app/**

- Source code in [GITLAB](https://gitlab.com/iles-de-paix/cartography_api_json_server): **https://gitlab.com/iles-de-paix/cartography_api_json_server**

## Deploy

[VERCEL](https://vercel.com/iles-de-pais/cartography-api-json-server)

## Description

API created with [JSON Server](https://www.npmjs.com/package/json-server) with only one dedicated file called **db.json** to manage the data as a database.

## Authors and acknowledgment

- [Loïc Burnotte](https://gitlab.com/loic.burnotte)
- [SofiDevO](https://github.com/SofiDevO/alurageek-API?tab=readme-ov-file)
- [Ivo-culic](https://ivo-culic.medium.com/create-restful-api-with-json-server-and-deploy-it-to-vercel-d56061c1157a)

## License

Open source project.
